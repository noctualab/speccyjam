﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class RithmGameGlue : MonoBehaviour {

	// Use this for initialization
    public List<ZXSprite> Tacchette;
    public RithmGame rgame;
    public TemperatureLevel t;
    public Text temp;
    public Text Rate;
    public Text result;
    public GameObject Pointer;
    public GameObject toDestroy;

    public Color ColorWrong = new Color(202.0f / 255.0f, 0.0f, 0.0f);
    public Color ColorRight = new Color(0.0f, 202.0f / 255.0f, 0.0f);

    private int ir = 0;

    private float TotalHits = 0.0f;
    private float ValidHits = 0.0f;
    private float CurrentHits = 0.0f;

    private int cycles = 0;

	void Start () {

        //removing minerals
        TestLogic.pause = true;
        foreach (var m in TheMessenger.weapon.Recipe.Keys) { 
            TestLogic.bsg.CurrentInventory.removeMetal(m,TheMessenger.weapon.Recipe[m]);
        }
        TotalHits = TheMessenger.weapon.Pattern * 4;
        rgame.InitializeRandomPattern(TheMessenger.weapon.Pattern);
        updateTacchette();
        StartCoroutine("GuiUpdated");
        StartCoroutine("Loop");
	}

    void updateTacchette()
    {
        for (int i = 0; i < 32; i++)
        {
            if (rgame.pattern[i])
            {
                Tacchette[i].Selected();
            }
            if (!rgame.pattern[i]) Tacchette[i].UnSelected();

        }
    }
    void updatePointerPos(int index){
        
        Pointer.transform.position = Tacchette[index].transform.position + new Vector3(0.0f, 18.0f, 0.0f);
            
    }

    IEnumerator GuiUpdated()
    {
        while (true)
        {
            temp.text = ((int)t.TLevel).ToString();

            if (((int)t.TLevel) < TheMessenger.weapon.Temperature)
            {
                temp.color = ColorWrong;
            }
            else
            {
                temp.color = ColorRight;
            }
            yield return new WaitForSeconds(0.2f);
        }
    }


    IEnumerator Loop(){
        updateTacchette();
        yield return new WaitForSeconds(5.0f);
        while (cycles<4)
        {

            if (rgame.pattern[ir])
            {
                CurrentHits++;
            }
            if (ir < 31)
            {
                ir++;
            }
            else
            {
                ir = 0;
                updateTacchette();
                cycles++;
            }
            updatePointerPos(ir);
           
            Rate.text = ((int)((ValidHits / CurrentHits) * 100)).ToString()+" %";
            if (((int)((ValidHits / CurrentHits) * 100))<0)
            {
            }
            Debug.Log(ValidHits / CurrentHits);
            Debug.Log("CURR" + CurrentHits);
            yield return new WaitForSeconds(0.2f);
            
        }
        if (((int)((ValidHits / CurrentHits) * 100)) > 60)
        {
            result.text = "Success!";
            TestLogic.bsg.CurrentInventory.Contents.Add(new InventoryWeaponSlot()
            {
                StoredWeapon = TheMessenger.weapon,
                WQuality = Weapon.Quality.EPIC
            }
            );
            TestLogic.bsg.Forged++;
            if (((int)((ValidHits / CurrentHits) * 100)) > 60)
            {
                TestLogic.bsg.Superior++;
            }
                
        }
        else
        {
            result.text = "FAIL!";
        }
        StopCoroutine("GuiUpdated");
        TheMessenger.weapon = null;
        yield return new WaitForSeconds(3.0f);
        SceneManager sm = Object.FindObjectOfType<SceneManager>() as SceneManager;
        sm.reloadSceneHierarchy();
        TestLogic.pause = false;
        Object.Destroy(toDestroy);

    }

    

    IEnumerator pointerDown(){
        Pointer.transform.position = Pointer.transform.position + new Vector3(0.0f, -10.0f, 0.0f);
        yield return new WaitForSeconds(0.1f);
        
    }

	// Update is called once per frame
	void Update () {
        

        if (Input.GetKeyDown(KeyCode.Return))
        {

            if (rgame.pattern[ir] && ((int)t.TLevel) >= TheMessenger.weapon.Temperature)
            {
                Debug.Log("---------------->" + ValidHits);
                ValidHits += 1.0f;
            }
            else
            {
                Debug.Log("FAIL");
                ValidHits -= 0.5f;
                if (ValidHits < 0) ValidHits = 0;
            }
            StartCoroutine(pointerDown());
        }
	
	}
}
