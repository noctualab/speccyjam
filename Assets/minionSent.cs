﻿using UnityEngine;
using System.Collections;

public class minionSent : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("goToNext");   
	}

    IEnumerator goToNext()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager sm = Object.FindObjectOfType<SceneManager>() as SceneManager;
        sm.reloadSceneHierarchy();
        TheMessenger.SendingMinion = false;
        Object.Destroy(gameObject);
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
