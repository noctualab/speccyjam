﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class FSM_InventoryManager : MonoBehaviour {

    public List<Text> Names;
    public List<Text> Qnt;
    
    public List<GameObject> Records;
    private int index = 0;
    private int pages = 0;

    private List<int> itemsPerPage = new List<int>();
    private int NumberOfPages;

	void Start () {
	
	}


    void OnEnable(){
        NumberOfPages = TestLogic.bsg.CurrentInventory.Contents.Count/5;
        for (int i = 0; i < NumberOfPages; i++)
        {
            itemsPerPage.Add(5);
        }
        if (TestLogic.bsg.CurrentInventory.Contents.Count % 5 > 0)
        {
            itemsPerPage.Add(TestLogic.bsg.CurrentInventory.Contents.Count % 5);
        }
        if (TestLogic.bsg.CurrentInventory.Contents.Count % 5 != 0)
            NumberOfPages++;

        index = 0;
        updateData();
        StartCoroutine("FSM_Loop");

    }

    void ClearData()
    {
        for (int i = 0; i < 5; i++)
        {
            Names[i].text = ""; Qnt[i].text = "";
            Records[i].SetActive(false);
        }
    }

    void updateData()
    {
        ClearData();
        if (TestLogic.bsg.CurrentInventory.Contents.Count == 0) return;
        for (int i = 0; i < itemsPerPage[pages]; i++)
        {
            var item = TestLogic.bsg.CurrentInventory.Contents[i + pages * 5];
            if(item is InventoryWeaponSlot){
                InventoryWeaponSlot iw = item as InventoryWeaponSlot;
                Names[i].text = iw.StoredWeapon.Name;
            }

            if (item is InventoryMetalSlot)
            {
                InventoryMetalSlot im = item as InventoryMetalSlot;
                Names[i].text = im.StoredMetal.Name;
                Qnt[i].text = im.Amount.ToString();
            }
            
            Records[i].SetActive(true);
        }
    }


    IEnumerator FSM_Loop()
    {
        while (true)
        {
            //if (Input.GetKeyDown(KeyCode.DownArrow))
            //{
            //    if (index < minionsPerPage[pages] - 1)
            //    {
            //        revertToBlack();
            //        index++;
            //        updateData();
            //    }
            //}
            //else if (Input.GetKeyDown(KeyCode.UpArrow))
            //{
            //    if (index > 0)
            //    {
            //        revertToBlack();
            //        index--;
            //        updateData();
            //    }

            //}

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (pages > 0)
                {
                    
                    index = 0;
                    pages--;

                    updateData();
                }

            }


            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (pages < NumberOfPages - 1)
                {
                    
                    index = 0;
                    pages++;
                    updateData();
                }

            }

            else if (Input.GetKeyDown(KeyCode.Return))
            {
               
            }
            
            yield return new WaitForSeconds(0.01f);
        }
    }


    void OnDisable(){

    }


	void Update () {
	
	}
}
