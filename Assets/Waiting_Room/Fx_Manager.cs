﻿using UnityEngine;
using System.Collections;

public class Fx_Manager : MonoBehaviour {

	public float SecondsBetweenTicks;
	public AudioClip Ticks;
	public AudioClip DayPassed;

    public AudioClip success;
    public AudioClip death;
    public AudioClip fail;
    
    
	public AudioSource audiosource;
	BSTime gameTime = new BSTime();
	public BSTimer Timer;


	public bool ShouldPlayTicks=true;
	void Start () {

        TestLogic.bsg.GameTime.DayPassed += PlayDayJingle;
        TestLogic.bsg.MiningSuccess += successFanfare;
        TestLogic.bsg.MiningFailure += failFanfare;
        StartCoroutine ("playTick");
		StartCoroutine ("timerized");
	
	}

	IEnumerator timerized(){
		while (true) {

			gameTime.Step ();
			yield return new WaitForSeconds(1.0f);
		}
	}
	IEnumerator playTick(){
		while (true) {
			if (ShouldPlayTicks) {
				audiosource.PlayOneShot (Ticks);
			}
			yield return new WaitForSeconds (SecondsBetweenTicks);
		}
	}
	public void PlayDayJingle(){
		audiosource.PlayOneShot (DayPassed);
	}

    public void successFanfare(MiningEvent v)
    {
        audiosource.PlayOneShot(success);
    }
    
    
    public void failFanfare(MiningEvent v)
    {
        if(v.Type==MiningEvent.EventType.CRITICAL_COMBAT_FAILURE)
        {
            audiosource.PlayOneShot(death);
        }
        else
        {
            audiosource.PlayOneShot(fail);
        }
    
    }

 
	void Update () {


	}
}
