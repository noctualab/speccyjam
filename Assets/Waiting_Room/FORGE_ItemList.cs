﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FORGE_ItemList  {
	public List<forgeItem> items = new List<forgeItem> {
		new forgeItem(){n="hammer",d="easy"},
		new forgeItem(){n="pickaxe",d="medium"},
		new forgeItem(){n="axe",d="hard"},
		new forgeItem(){n="sword",d="easy"},
		new forgeItem(){n="scissors",d="medium"},
		new forgeItem(){n="nail",d="medium"},
		new forgeItem(){n="shovel",d="easy"},
		new forgeItem(){n="idol",d="hard"},
		new forgeItem(){n="coin",d="easy"},
		new forgeItem(){n="gold coin",d="easy"},
		new forgeItem(){n="antichrist",d="evil"}
		};
		public int pages=3;
	public List<int> itemPerPage = new List<int> {
		5,5,1
		};
	}

public class forgeItem{
	public string n;
	public string d;
}
