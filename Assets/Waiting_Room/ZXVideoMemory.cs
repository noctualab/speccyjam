﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class ZXVideoMemory : MonoBehaviour {

	// Use this for initialization
	public Texture2D videoMemoryPAPER;
	public Texture2D videoMemoryINK;

	public ZXColor INK;
	public ZXColor PAPER;
	public bool bright = false;
	private Color[] _ink = new Color[24*32];
	private Color[] _paper = new Color[24*32];
	void Start () {
//		StartCoroutine ("refresh");
	}
	
	// Update is called once per frame

	IEnumerator refresh(){
		while (true) {
			for (int i = 0; i < 24 * 32; i++) {
				if (bright == false) {
					_ink [i] = INK.color;
					_paper [i] = PAPER.color;
				} else {
					_ink [i] = INK.colorBright;
					_paper [i] = PAPER.colorBright;
				}
			}
			//videoMemoryINK.SetPixels (0, 0, 32, 24, _ink);
			videoMemoryPAPER.SetPixels (0, 0, 32, 24, _paper);
//			videoMemoryINK.Apply ();
			videoMemoryPAPER.Apply ();
			yield return new WaitForSeconds (0.04f);
		}
	}
	void Update () {
		for (int i = 0; i < 24 * 32; i++) {
			if (bright == false) {
				_ink [i] = INK.color;
				_paper [i] = PAPER.color;
			} else {
				_ink [i] = INK.colorBright;
				_paper [i] = PAPER.colorBright;
			}
		}
////		videoMemoryINK.SetPixels (0, 0, 32, 24, _ink);
		videoMemoryPAPER.SetPixels (0, 0, 32, 24, _paper);
////		videoMemoryINK.Apply ();
		videoMemoryPAPER.Apply (); 
	}
	

	void setBorderColor(ZXColor color){
		Camera.main.backgroundColor = color.color;
	}

	void LateUpdate(){
		ClearVideoMemoryToDefault ();
	}

	void ClearVideoMemoryToDefault(){

	}
}
