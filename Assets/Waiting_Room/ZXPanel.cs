﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ZXPanel : MonoBehaviour {

	// Use this for initialization
	public Texture2D cornerSprite_BL;
	public Texture2D cornerSprite_BR;
	public Texture2D cornerSprite_TL;
	public Texture2D cornerSprite_TR;
	public Texture2D sideSprite_B;
	public Texture2D sideSprite_T;
	public Texture2D sideSprite_L;
	public Texture2D sideSprite_R;
	public Texture2D innerSprite;

	public Texture2D INK;
	public Texture2D PAPER;
	public ZXVideoMemory ZXVideoMemory;

	public int width;
	public int height;

	private SpriteRenderer sr;
	void Start () {


	
	}

	void OnEnable(){
		ZXVideoMemory = Object.FindObjectOfType<ZXVideoMemory> () as ZXVideoMemory;
		sr = gameObject.GetComponent<SpriteRenderer> () as SpriteRenderer;

		Texture2D panelTexture = new Texture2D (width * 8, height * 8, TextureFormat.RGBA32, false);
		//ALPA INIT
		Color[] transparentColor = new Color[width * 8* height * 8];
		for (int i = 0; i < width * 8 * height * 8; i++) {
			transparentColor [i] = new Color (0.0f, 0.0f, 0.0f, 0.0f);
		}
		panelTexture.SetPixels (transparentColor);

		//FILTERING
		panelTexture.filterMode = FilterMode.Point;

		//CORNERS
		panelTexture.SetPixels (0, 0, 8, 8, cornerSprite_BL.GetPixels ());
		panelTexture.SetPixels (width * 8-8, 0, 8, 8, cornerSprite_BR.GetPixels ());
		panelTexture.SetPixels (0, height * 8-8, 8, 8, cornerSprite_TL.GetPixels ());
		panelTexture.SetPixels (width * 8-8, height * 8-8, 8, 8, cornerSprite_TR.GetPixels ());

		//TOP/BOTTOM SIDES
		for (int i = 1; i < width-1; i++) {
			panelTexture.SetPixels (i * 8, 0, 8, 8, sideSprite_B.GetPixels ());
			panelTexture.SetPixels (i * 8, height*8-8, 8, 8, sideSprite_T.GetPixels ());
		}

		//LEFT/RIGHT SIDES
		for (int i = 1; i < height-1; i++) {
			panelTexture.SetPixels (0, i*8, 8, 8, sideSprite_L.GetPixels ());
			panelTexture.SetPixels (width*8-8, i*8, 8, 8, sideSprite_R.GetPixels ());
		}

		//THE INSIDE
		for (int i = 1; i < width - 1; i++) {
			for (int j = 1; j < height - 1; j++) {
				panelTexture.SetPixels (i * 8, j * 8, 8, 8, innerSprite.GetPixels ());
			}
		}

		panelTexture.Apply ();

		Sprite panel = Sprite.Create (panelTexture, new Rect (0, 0, width * 8, height * 8), new Vector2 (0,1),1);

		sr.sprite = panel;

		StartCoroutine ("animationLoop"); 
	}

	void OnDisable(){
		StopAllCoroutines ();
	}

	IEnumerator animationLoop(){
		while (true) {

			for (int i = 0; i < width ; i++) {
				for (int j = 0; j < height ; j++) {

					ZXVideoMemory.videoMemoryINK.SetPixels (((int)(transform.position.x + 128 + i * 8) >> 3), (((int)transform.position.y + 96 -j*8-1) >> 3), 1, 1, INK.GetPixels ());
					ZXVideoMemory.videoMemoryPAPER.SetPixels (((int)(transform.position.x + 128 + i * 8) >> 3), (((int)transform.position.y + 96-j*8-1) >> 3), 1, 1, PAPER.GetPixels ());
					ZXVideoMemory.videoMemoryINK.Apply ();
					ZXVideoMemory.videoMemoryPAPER.Apply ();
				}
			} 

			yield return new WaitForSeconds (0.05f);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
