﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ZXColor {
    
    public Color color;
    public Color colorBright;
    
}
