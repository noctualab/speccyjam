﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class FSM_OrdersManager : MonoBehaviour
{

    public List<Text> Names;

    public List<GameObject> Records;
    public List<Weapon> weaponList=new List<Weapon>();
    public AudioClip sell;
    private int index = 0;
    private int pages = 0;
    public Color RedDim = new Color(255.0f / 205.0f, 0.0f, 0.0f);
    private List<int> itemsPerPage = new List<int>();
    private int NumberOfPages;
    
    void Start()
    {

    }


    void updateTempInventory()
    {
        weaponList.Clear();
        itemsPerPage.Clear();
        foreach (var item in TestLogic.bsg.CurrentInventory.Contents)
        {
            if (item is InventoryWeaponSlot)
            {
                var w = item as InventoryWeaponSlot;
                weaponList.Add(w.StoredWeapon);
            }
        }
        updateIndeces();
        index = 0;
    }

    void updateIndeces()
    {
        NumberOfPages = weaponList.Count / 5;
        for (int i = 0; i < NumberOfPages; i++)
        {
            itemsPerPage.Add(5);
        }
        if (weaponList.Count % 5 > 0)
        {
            itemsPerPage.Add(weaponList.Count % 5);
        }
        if (weaponList.Count % 5 != 0)
            NumberOfPages++;

    }


    void OnEnable()
    {

        updateTempInventory();
        
        index = 0;
        updateData();
        StartCoroutine("FSM_Loop");

    }

    void ClearData()
    {
        for (int i = 0; i < 5; i++)
        {
            Names[i].text = "";

            Records[i].SetActive(false);
        }
    }

    void updateData()
    {
        ClearData();
        if (weaponList.Count == 0) return;
        for (int i = 0; i < itemsPerPage[pages]; i++)
        {
            Names[i].text = weaponList[i + pages * 5].Name;
            Records[i].SetActive(true);
        }
    }

    void revertToBlack()
    {
        Names[index].color = Color.black;
    }

    void highlight()
    {
        Names[index].color = RedDim;
    }

    IEnumerator FSM_Loop()
    {
        while (true)
        {

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (index < itemsPerPage[pages] - 1)
                {
                    revertToBlack();
                    index++;
                    updateData();
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (index > 0)
                {
                    revertToBlack();
                    index--;
                    updateData();
                }

            }

            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (pages > 0)
                {

                    index = 0;
                    pages--;

                    updateData();
                }

            }


            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (pages < NumberOfPages - 1)
                {

                    index = 0;
                    pages++;
                    updateData();
                }

            }

            else if (Input.GetKeyDown(KeyCode.Return))
            {
                TestLogic.bsg.Gold += weaponList[index + pages * 5].SellValue;
                TestLogic.bsg.Fame += (int)(Mathf.Ceil(weaponList[index + pages * 5].SellValue) * 0.1f) ;
                InventorySlot toBeRemoved = null;
                foreach (var i in TestLogic.bsg.CurrentInventory.Contents)
                {
                    if (i is InventoryWeaponSlot)
                    {
                        InventoryWeaponSlot iws = i as InventoryWeaponSlot;
                        if (iws.StoredWeapon == weaponList[index + pages * 5])
                        {
                            toBeRemoved = i;
                            
                        }
                    }
                }
                TestLogic.bsg.CurrentInventory.Contents.Remove(toBeRemoved);
                updateTempInventory();
                updateData();
                AudioSource audios =  Camera.main.GetComponent<AudioSource>() as AudioSource;
                audios.PlayOneShot(sell);
            }
            highlight();
            yield return new WaitForSeconds(0.01f);
        }
    }


    void OnDisable()
    {

    }


    void Update()
    {

    }
}
