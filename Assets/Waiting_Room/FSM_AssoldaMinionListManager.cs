﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
public class FSM_AssoldaMinionListManager : MonoBehaviour {


	public List<Text> Names;
    public List<Text> Probs;
	public Color RedDim = new Color (255.0f / 205.0f, 0.0f, 0.0f);
    public List<GameObject> Records;
	private int index = 0;
    private int pages = 0;
    private List<int> minionsPerPage = new List<int>();
    private int NumberOfPages;

    public List<Minion> minionList;

	void Start () {

	}
	
	void OnEnable(){

        minionList = TestLogic.bsg.AvailableMinion;
        

        NumberOfPages = minionList.Count / 5;
        for (int i = 0; i < NumberOfPages; i++){
            minionsPerPage.Add(5);
        }
        if (minionList.Count % 5 > 0)
        {
            minionsPerPage.Add(minionList.Count % 5);
        }
        if (minionList.Count % 5 != 0)
            NumberOfPages++;
        

        index = 0;
		Names [index].color = RedDim;
        updateData();	
		StartCoroutine ("FSM_Loop");

	}

	void OnDisable(){
		StopAllCoroutines ();
	}

	void revertToBlack(){
		Names [index].color = Color.black;
	}

    void ClearData()
    {
        for (int i = 0; i < 5; i++)
        {
            Names[i].text = ""; Probs[i].text = "";
            Records[i].SetActive(false);
        }
    }

    void updateData()
    {
        ClearData();
        if (minionList.Count == 0) return;
        for (int i = 0; i < minionsPerPage[pages]; i++)
        {
            var minion = minionList[i + pages * 5];
            Names[i].text = minion.Name;
            if (TheMessenger.SendingMinion){
                Probs[i].text = ((int)(TheMessenger.location.EstimateSuccess(minion) * 100)).ToString();

            }
            Records[i].SetActive(true);
        }
    }

	void highlight(){
		Names [index].color = RedDim;
	}
	IEnumerator FSM_Loop(){
		while (true) {
			if(Input.GetKeyDown(KeyCode.DownArrow)){
				if (index < minionsPerPage[pages]-1) {
					revertToBlack ();
					index++;
                    updateData();
				}
			}
			else if(Input.GetKeyDown(KeyCode.UpArrow)){
				if (index > 0) {
					revertToBlack ();
					index--;
                    updateData();	
				}

			}

            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (pages > 0)
                {
                    revertToBlack();
                    index = 0;
                    pages--;

                    updateData();
                }

            }


            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (pages < NumberOfPages - 1)
                {
                    revertToBlack();
                    index = 0;
                    pages++;
                    updateData();
                }

            }

			else if(Input.GetKeyDown(KeyCode.Return)){
                if (minionList.Count != 0) { 
                SceneManager sm = Object.FindObjectOfType<SceneManager>() as SceneManager;
                TheMessenger.theMinion = minionList[index + pages * 5];
                TheMessenger.PreviousScene = "AssoldaMinion";
                sm.addScene("MinionDetail");
                Object.Destroy(gameObject);
                TheMessenger.HiringMinion = true;
                }
                
			}
			highlight ();
			yield return new WaitForSeconds (0.01f);
		}
	}


	void Update () {
	
	}
}
