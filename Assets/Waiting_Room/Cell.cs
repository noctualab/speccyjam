﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Cell{

    public ZXColor foreground;
    public ZXColor background;
    public bool flashing;
    public bool bright;
}
