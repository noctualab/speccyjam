﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
public class FSM_MapListManager : MonoBehaviour {


	public ZXSprite Map;
	private int index = 0;
	private bool inConfirmationPhase=false;
	private BackToScene bts;
	public GameObject panels;
	public GameObject texts;

    public Text locationName;
    public Text metalName;
    public Text time;
    public Text threat;

	void Start () {

	}
	
	void OnEnable(){
        TheMessenger.toMainScene = true;
		bts = gameObject.GetComponent<BackToScene> () as BackToScene;
		bts.enabled = true;
		panels.SetActive(false);
		texts.SetActive(false);
		index = 0;
		Map.currentAnimationIndex = index;
		StartCoroutine ("FSM_Loop");

	}

	void OnDisable(){
		StopAllCoroutines ();
	}


	IEnumerator FSM_Loop(){
		while (true) {
			if (inConfirmationPhase == false) {
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
					if (index > 0)
						index--;
					Map.currentAnimationIndex = index;
				} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
					if (index < 4)
						index++;
					Map.currentAnimationIndex = index;
				} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					if (index > 0)
						index--;
					Map.currentAnimationIndex = index;
				} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
					if (index < 4)
						index++;
					Map.currentAnimationIndex = index;
				} else if (Input.GetKeyDown (KeyCode.Return)) {
                    Location l = Location.Locations.ElementAt(index).Value;
                    locationName.text = l.Name;
                    metalName.text = l.PossibleMetals.ElementAt(0).Key;
                    threat.text = l.EnemyLevel.ToString();
                    time.text = l.ExpeditionDuration.ToString();

					inConfirmationPhase = true;
					panels.SetActive(true);
					texts.SetActive(true);
					bts.enabled = false;
                    yield return new WaitForSeconds(0.001f);
				}
			}
			if (inConfirmationPhase) {
				if (Input.GetKeyDown (KeyCode.Return)) {
                    TheMessenger.SendingMinion = true;
                    TheMessenger.PreviousScene = "Map";
                    TheMessenger.toMainScene = false;
                    TheMessenger.location = Location.Locations.ElementAt(index).Value;
                    Application.LoadLevelAdditive("Minions");
                    Object.Destroy(gameObject);
				}

				else if (Input.GetKeyDown (KeyCode.Backspace)) {
                    TheMessenger.toMainScene = true ;
                    TheMessenger.SendingMinion = false;
					inConfirmationPhase = false;
					panels.SetActive(false);
					texts.SetActive(false);
					bts.enabled = true;
				}

			}

			yield return new WaitForSeconds (0.001f);
		}
	}


	void Update () {
	
	}
}
