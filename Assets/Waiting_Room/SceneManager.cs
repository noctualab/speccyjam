﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

	GameObject mainGOtoDisable ;
    public GameObject Logic;
    public GameObject Panels;
	void Start () {
		mainGOtoDisable= GameObject.FindGameObjectWithTag ("mainSceneToDisable");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void changeScene(string name){
		Application.LoadLevel(name);
	}

	public void addScene(string name){
		Application.LoadLevelAdditive(name);
		mainGOtoDisable.SetActive (false);
	}

    public void gameOver()
    {
        Panels.SetActive(false);
        mainGOtoDisable.SetActive(false);
    }
	public void reloadSceneHierarchy(){
		mainGOtoDisable.SetActive (true);
	
	}
}
