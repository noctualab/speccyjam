﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FSM_MAINMENU : MonoBehaviour {

	public List<ZXSprite> MainMenu;
	private int index = 0;

	void OnEnable(){
		StartCoroutine ("FSM_Loop");
		index = 0;
		MainMenu [0].Selected ();
	}

	void OnDisable(){
		StopAllCoroutines ();
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	IEnumerator FSM_Loop(){
		while (true) {
			Debug.Log("SELECTED: "+index);

			if(Input.GetKeyDown(KeyCode.LeftArrow)){
				MainMenu [index].UnSelected ();
				if (index == 1)
					index = 0;
				else if (index == 3)
					index = 2;
				else if (index == 5)
					index = 4;
				MainMenu [index].Selected ();
			}
			else if(Input.GetKeyDown(KeyCode.RightArrow)){
				MainMenu [index].UnSelected ();
				if (index == 0)
					index = 1;
				else if (index == 2)
					index = 3;
				else if (index == 4)
					index = 5;
				MainMenu [index].Selected ();
			}
			else if(Input.GetKeyDown(KeyCode.UpArrow)){
				MainMenu [index].UnSelected ();
				if (index == 2)
					index = 0;
				else if (index == 3)
					index = 1;
				else if (index == 4)
					index = 2;
				else if (index == 5)
					index = 3;
                else if (index == 6)
                    index = 5;
				MainMenu [index].Selected ();
			}
			else if(Input.GetKeyDown(KeyCode.DownArrow)){
				MainMenu [index].UnSelected ();
				if (index == 0)
					index = 2;
				else if (index == 1)
					index = 3;
				else if (index == 2)
					index = 4;
				else if (index == 3)
					index = 5;
                else if (index == 5)
                    index = 6;
				MainMenu [index].Selected ();
			}
			else if(Input.GetKeyDown(KeyCode.Return)){
				MainMenu [index].UnSelected ();
				MainMenu [index].Event ();
			}

			yield return new WaitForSeconds (0.001f);
		}
	}
}


