﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZXScreen : MonoBehaviour {

    public ScreenGrid screengrid;
    public ZXColor borderColor;

    public static Dictionary<string, ZXColor> colors = new Dictionary<string, ZXColor>(){
        {"black",new ZXColor(){color=new Color(0.0f,0.0f,0.0f),colorBright=new Color(0.0f,0.0f,0.0f)}},
        {"blue",new ZXColor(){color=new Color(0.0f,0.0f,205.0f/255.0f),colorBright=new Color(0.0f,0.0f,1.0f)}},
        {"red",new ZXColor(){color=new Color(205.0f/255.0f,0.0f,0.0f),colorBright=new Color(1.0f,0.0f,0.0f)}},
        {"magenta",new ZXColor(){color=new Color(205.0f/255.0f,0.0f,205.0f/255.0f),colorBright=new Color(1.0f,0.0f,1.0f)}},
        {"green",new ZXColor(){color=new Color(0.0f,205.0f/255.0f,0.0f),colorBright=new Color(0.0f,1.0f,0.0f)}},
        {"cyan",new ZXColor(){color=new Color(0.0f,205.0f/255.0f,205.0f/255.0f),colorBright=new Color(0.0f,1.0f,1.0f)}},
        {"yellow",new ZXColor(){color=new Color(205.0f/255.0f,205.0f/255.0f,0.0f),colorBright=new Color(1.0f,1.0f,0.0f)}},
        {"white",new ZXColor(){color=new Color(205.0f/255.0f,205.0f/255.0f,205.0f/255.0f),colorBright=new Color(1.0f,1.0f,1.0f)}}
    };

    public Sprite spriteImage;
    public SpriteRenderer sr;

    //Speccy target framerate
    void Awake()
    {
        Application.targetFrameRate = 25;
    }


    // Use this for initialization
	void Start () {
        borderColor = colors["magenta"];
        setBorderColor(borderColor);
        sr.sprite = spriteImage;
        
	}
	
	// Update is called once per frame
	void Update () {
            for (int rows = 0; rows < 24; rows++)
            {
                for (int cols = 0; cols < 32; cols++)
                {
                    var c = new Color[8 * 8];
                    for (var i = 0; i < 8 * 8; i++)
                    {
                        c[i] = colors["red"].color;
                    }
                    spriteImage.texture.SetPixels(cols*8, rows*8, 8, 8, c);
                    }
            } 
                spriteImage.texture.Apply();
	}

    void setBorderColor(ZXColor color)
    {
        Camera.main.backgroundColor = color.color;
    }
}
