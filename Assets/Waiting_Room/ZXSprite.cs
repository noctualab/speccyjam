﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ZXSprite : MonoBehaviour {

	private ZXVideoMemory ZXVideoMemory;
	public List<ZXSpriteStrip> spriteData;
	public int xFrame;
	public int yFrame;

	public string LoadedScene;

	public bool flashing = false; //invert the PAPER/INK 
	public bool bright = false; //select the bright strip

	[Range(0.0f,0.1f)]
	public float drawRandomDelay;
	public float drawBaseDelay = 0.0f;


	public int currentAnimationIndex;
	public int tileSize = 8; //DONT TOUCH ME
	private int currentAnimationFrame=0;
	private SpriteRenderer sr;

	private bool flashToggler;

	void Start () {
	
	}

	void OnEnable(){
		ZXVideoMemory = Object.FindObjectOfType<ZXVideoMemory> () as ZXVideoMemory;
		sr = gameObject.GetComponent<SpriteRenderer> () as SpriteRenderer;
		StartCoroutine ("animationLoop"); 
	}

	void OnDisable(){
		StopAllCoroutines ();
	}

	void LateUpdate () { 
		ZXVideoMemory.videoMemoryINK.Apply ();	
		ZXVideoMemory.videoMemoryPAPER.Apply ();	
	}
		
	public void Selected(){
		currentAnimationIndex = 1;
	}

    public void Selected(bool b)
    {
        if(b)
            currentAnimationIndex = 1;
        if(!b)
            currentAnimationIndex = 0;
    }

	public void UnSelected(){
		currentAnimationFrame = 0;
		currentAnimationIndex = 0;
	}

	public void Event(){
		SceneManager sm = ZXVideoMemory.GetComponent<SceneManager> ();
		sm.addScene (LoadedScene);
	}


	IEnumerator animationLoop(){
		while (true) {
			writeToVideoMemory ();
			//yield return new WaitForSeconds (0.04f+Random.Range(0.0f,drawRandomDelay)+drawBaseDelay); //25fps + dirtiest trick ever
			Debug.Log ("Animation frame index : " + currentAnimationFrame);

			if (currentAnimationFrame >=spriteData [currentAnimationIndex].spriteStrip.Count-1) {
				currentAnimationFrame = 0;
			}
			else {
				currentAnimationFrame++;
			}
			//-----------------------IMPORTANT-----------------------------
			//CRITICAL POINT, FAILURE HERE
			sr.sprite = spriteData [currentAnimationIndex].spriteStrip[currentAnimationFrame];
			yield return new WaitForSeconds (spriteData [currentAnimationIndex].duration);

		}
	}

	int pixelToTileCoordinates(int x){
		return  x >> 3;
	}

	/// <summary>
	/// I have to write just the colors data to the video memory.
	/// </summary>
	void writeToVideoMemory(){
		//OPTIMIZE THIS
		if (flashing) {
			if (flashToggler == true) {
				//BODY
				writeInkMemory();
				writePaperMemory();
				//END
				flashToggler = false;
			} else if (flashToggler == false) {
				//BODY
				writeInkMemoryInverse();
				writePaperMemoryInverse();
				//END
				flashToggler = true;
			}
		}

		if (flashing == false && bright == false) {

			writeInkMemory();
			writePaperMemory();
		}
	}

	void writeInkMemory(){
		//BASE UPDATE
		ZXVideoMemory.videoMemoryINK.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			xFrame >> 3, yFrame >> 3,
			spriteData [currentAnimationIndex].inkStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, yFrame>>3));
		//RIGHT-EST COLUMN
		ZXVideoMemory.videoMemoryINK.SetPixels (
			((int)(transform.position.x+128+xFrame-1) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			1, yFrame >> 3,
			spriteData [currentAnimationIndex].inkStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, 1, yFrame>>3));
		//TOP-EST ROW
		ZXVideoMemory.videoMemoryINK.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-1)>> 3,
			xFrame>>3, 1,
			spriteData [currentAnimationIndex].inkStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, 1));
		//TOP-RIGHT BLOCk
		ZXVideoMemory.videoMemoryINK.SetPixel (
			((int)(transform.position.x + 128 + xFrame - 1) >> 3), ((int)transform.position.y + 96 - 1) >> 3,
			spriteData [currentAnimationIndex].inkStrip.GetPixel ((currentAnimationFrame * xFrame >> 3), 0));

//		ZXVideoMemory.videoMemoryINK.Apply ();		
	}

	void writePaperMemory(){
		//BASE UPDATE
		ZXVideoMemory.videoMemoryPAPER.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			xFrame >> 3, yFrame >> 3,
			spriteData [currentAnimationIndex].paperStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, yFrame>>3));
		//RIGHT-EST COLUMN
		ZXVideoMemory.videoMemoryPAPER.SetPixels (
			((int)(transform.position.x+128+xFrame-1) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			1, yFrame >> 3,
			spriteData [currentAnimationIndex].paperStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, 1, yFrame>>3));
		//TOP-EST ROW
		ZXVideoMemory.videoMemoryPAPER.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-1)>> 3,
			xFrame>>3, 1,
			spriteData [currentAnimationIndex].paperStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, 1));
		//TOP-RIGHT BLOCk
		ZXVideoMemory.videoMemoryPAPER.SetPixel (
			((int)(transform.position.x + 128 + xFrame - 1) >> 3), ((int)transform.position.y + 96 - 1) >> 3,
			spriteData [currentAnimationIndex].paperStrip.GetPixel ((currentAnimationFrame * xFrame >> 3), 0));

//		ZXVideoMemory.videoMemoryPAPER.Apply ();		
	}

	void writeInkMemoryInverse(){
		//BASE UPDATE
		ZXVideoMemory.videoMemoryPAPER.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			xFrame >> 3, yFrame >> 3,
			spriteData [currentAnimationIndex].inkStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, yFrame>>3));
		//RIGHT-EST COLUMN
		ZXVideoMemory.videoMemoryPAPER.SetPixels (
			((int)(transform.position.x+128+xFrame-1) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			1, yFrame >> 3,
			spriteData [currentAnimationIndex].inkStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, 1, yFrame>>3));
		//TOP-EST ROW
		ZXVideoMemory.videoMemoryPAPER.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-1)>> 3,
			xFrame>>3, 1,
			spriteData [currentAnimationIndex].inkStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, 1));
		//TOP-RIGHT BLOCk
		ZXVideoMemory.videoMemoryPAPER.SetPixel (
			((int)(transform.position.x + 128 + xFrame - 1) >> 3), ((int)transform.position.y + 96 - 1) >> 3,
			spriteData [currentAnimationIndex].inkStrip.GetPixel ((currentAnimationFrame * xFrame >> 3), 0));
	
	}

	void writePaperMemoryInverse(){
		//BASE UPDATE
		ZXVideoMemory.videoMemoryINK.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			xFrame >> 3, yFrame >> 3,
			spriteData [currentAnimationIndex].paperStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, yFrame>>3));
		//RIGHT-EST COLUMN
		ZXVideoMemory.videoMemoryINK.SetPixels (
			((int)(transform.position.x+128+xFrame-1) >> 3), ((int)transform.position.y+96-yFrame)>> 3,
			1, yFrame >> 3,
			spriteData [currentAnimationIndex].paperStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, 1, yFrame>>3));
		//TOP-EST ROW
		ZXVideoMemory.videoMemoryINK.SetPixels (
			((int)(transform.position.x+128) >> 3), ((int)transform.position.y+96-1)>> 3,
			xFrame>>3, 1,
			spriteData [currentAnimationIndex].paperStrip.GetPixels ((currentAnimationFrame * xFrame >> 3), 0, xFrame>>3, 1));
		//TOP-RIGHT BLOCk
		ZXVideoMemory.videoMemoryINK.SetPixel (
			((int)(transform.position.x + 128 + xFrame - 1) >> 3), ((int)transform.position.y + 96 - 1) >> 3,
			spriteData [currentAnimationIndex].paperStrip.GetPixel ((currentAnimationFrame * xFrame >> 3), 0));

		//		ZXVideoMemory.videoMemoryPAPER.Apply ();			
	}
}
	
[System.Serializable]
public class ZXSpriteStrip{
	public List<Sprite> spriteStrip;
	public Texture2D inkStrip;
	public Texture2D paperStrip;
	public float duration=0.05f;
}