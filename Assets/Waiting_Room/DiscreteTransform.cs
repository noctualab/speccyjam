﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class DiscreteTransform : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		int x = (int) Mathf.Round(transform.position.x);
		int y = (int) Mathf.Round(transform.position.y);
		int z = (int) Mathf.Round(transform.position.z);
		transform.position = new Vector3(x,y,z);
	}
}
