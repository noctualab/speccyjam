﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
public class FSM_ForgeListManager : MonoBehaviour {


	public List<Text> Names;
	public List<Text> Difficulties;
    public List<GameObject> Records;
	public Color RedDim = new Color (255.0f / 205.0f, 0.0f, 0.0f);
	public FORGE_ItemList list = new FORGE_ItemList();

	private int index = 0;
	private int pages=0;


	void Start () {


	}
	
	void OnEnable(){
        
		index = 0;
		Names [index].color = RedDim;
		Difficulties [index].color = RedDim;
		updateData ();


		StartCoroutine ("FSM_Loop");

	}

	void ClearData(){
		for (int i = 0; i < 5; i++) {
			Names [i].text = "";
            Difficulties[i].text = "";
            Records[i].SetActive(false);
		}
	}

	void updateData(){
		ClearData ();
		for (int i = 0; i < Weapon.WeaponsPerPage[pages]; i++) {
				//Names [i].text = list.items [i + pages * 5].n;
                var item = Weapon.Weapons.ElementAt(i+pages*5);
                Weapon w = item.Value;
                Names[i].text = w.Name;
                Difficulties[i].text = w.SellValue+" $";
                Records[i].SetActive(true);
			}
	}

	void OnDisable(){
		StopAllCoroutines ();
	}

	void revertToBlack(){
		Names [index].color = Color.black;
		Difficulties [index].color = Color.black;
	}

	void highlight(){
		Names [index].color = RedDim;
		Difficulties [index].color = RedDim;
	}
	IEnumerator FSM_Loop(){
		while (true) {
			if(Input.GetKeyDown(KeyCode.DownArrow)){
                if (index < Weapon.WeaponsPerPage[pages] - 1)
                {
					revertToBlack ();
					index++;
					updateData ();
				}
			}
			else if(Input.GetKeyDown(KeyCode.UpArrow)){
				if (index > 0) {
					revertToBlack ();
					index--;
					updateData ();	
				}

			}

			else if(Input.GetKeyDown(KeyCode.LeftArrow)){
				if (pages > 0) {
					revertToBlack ();
					index = 0;
					pages--;

					updateData ();
				}

			}


			else if(Input.GetKeyDown(KeyCode.RightArrow)){
				if (pages < Weapon.NumberOfPages-1) {
					revertToBlack ();
					index = 0;
					pages++;
					updateData ();
				}

			}

			else if(Input.GetKeyDown(KeyCode.Return)){
                var item = Weapon.Weapons.ElementAt(index+pages*5);
                TheMessenger.WeaponName = item.Key;
                TheMessenger.PreviousScene = "Forge";
                Application.LoadLevelAdditive("ItemToForgeDetail");
                Object.Destroy(gameObject);
                    
			}
			highlight ();
			yield return new WaitForSeconds (0.01f);
		}
	}


	void Update () {
	
	}
}
