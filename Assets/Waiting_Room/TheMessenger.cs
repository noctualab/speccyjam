﻿using UnityEngine;
using System.Collections;

public static class TheMessenger  {

    public static string WeaponName;
    public static string PreviousScene;
    public static Minion theMinion;
    public static bool toMainScene = true;
    public static bool SendingMinion=false;
    public static bool HiringMinion=false;
    public static Location location;

    public static int gameover_DAYS = 0;
    public static int gameover_GLORY = 0;

    public static Weapon weapon;

}
