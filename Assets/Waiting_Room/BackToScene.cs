﻿using UnityEngine;
using System.Collections;

public class BackToScene : MonoBehaviour {

	// Use this for initialization
    public bool toMainScene = true;
	void Start () {
        toMainScene = TheMessenger.toMainScene;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Backspace)){
            if (toMainScene)
            {
			SceneManager sm = Object.FindObjectOfType<SceneManager> () as SceneManager;
			sm.reloadSceneHierarchy ();
			Destroy (gameObject);
            }
            else
            {
                SceneManager sm = Object.FindObjectOfType<SceneManager>() as SceneManager;
                Application.LoadLevelAdditive(TheMessenger.PreviousScene);
                Destroy(gameObject);
            }
		}
	}
}
