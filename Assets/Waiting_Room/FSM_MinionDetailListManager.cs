﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class FSM_MinionDetailListManager : MonoBehaviour {


	private int index = 0;
    public Text name;
    public Text combat;
    public Text geo;
    public Text salary;
    public Text status;
    
	void Start () {

	}
	
	void OnEnable(){
        name.text = TheMessenger.theMinion.Name;
        combat.text = TheMessenger.theMinion.Combat.ToString();
        geo.text = TheMessenger.theMinion.Geology.ToString();
        salary.text = (TheMessenger.theMinion.HireQuote/12).ToString();
        if (TheMessenger.theMinion.Available)
            status.text = "available";
        else
            status.text = "on mission";
        StartCoroutine("FSM_Loop");
	}

    IEnumerator FSM_Loop()
    {
        while (true)
        {

            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (TheMessenger.HiringMinion) { 
                SceneManager sm = Object.FindObjectOfType<SceneManager>() as SceneManager;
                TheMessenger.PreviousScene = "AssoldaMinion";
                TestLogic.bsg.HireMinion(TheMessenger.theMinion);
                sm.addScene("MinionHired");
                TheMessenger.HiringMinion = false;
                Object.Destroy(gameObject);
                }

            }
            
            yield return new WaitForSeconds(0.01f);
        }
    }


	void OnDisable(){
		StopAllCoroutines ();
	}




	void Update () {
	
	}
}
