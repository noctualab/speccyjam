﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BlackSmithData : MonoBehaviour {

    public Text itemForged;
    public Text itemsSuperior;
    public Text Glory;
    public Text Gold;
    public Text Expenses;

	void Start () {
        itemForged.text = TestLogic.bsg.Forged.ToString();
        itemsSuperior.text = TestLogic.bsg.Superior.ToString();
        Glory.text = TestLogic.bsg.Fame.ToString();
        Gold.text = TestLogic.bsg.Gold.ToString();
        Expenses.text = TestLogic.bsg.TotalExpenses().ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
