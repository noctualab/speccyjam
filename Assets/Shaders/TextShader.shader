﻿Shader "ZX/ZXTextShader"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_B ("Paper", 2D) = "white" {}
		_F ("Ink", 2D) = "white" {}
		[MaterialToggle] _toggle("Toggler", Float) = 0
    }
 
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
 
        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha
 
        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"
           
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };
 
            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
				float2 screenPos : TEXCOORD1;
            };
           
            fixed4 _Color;
 
            v2f vert(appdata_t IN)
            {
                v2f OUT;	
                OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color;
				OUT.screenPos = ComputeScreenPos(OUT.vertex);
                return OUT;
            }
  
            sampler2D _MainTex;
			sampler2D _B;
			sampler2D _F;
 			uniform float _toggle;

            fixed4 frag(v2f IN) : COLOR
            {
                half4 texcol = tex2D (_MainTex, IN.texcoord);              
               	if(_toggle!=0){
					if(texcol.a>0.5f)
					texcol=texcol.a*tex2D(_B,half2( IN.screenPos.x, IN.screenPos.y));
					else
					texcol=tex2D(_F,half2( IN.screenPos.x, IN.screenPos.y));
				}
				else{
				texcol=texcol.a*texcol;
				}
                return texcol;
            }
        ENDCG
        }
    }
    Fallback "Sprites/Default"
}
 