﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FSM_ForgeItemDetail : MonoBehaviour {

    public Canvas c;
    public Text name;
    public Text description;
    public Text worth;
    public Text recipe;
    public Text forge;
    public Text Temp;

    public bool canForge = false;

	void Start () {
	
	}

    void OnEnable()
    {
        Weapon w = Weapon.Weapons[TheMessenger.WeaponName];
        TheMessenger.weapon = w;
        name.text = w.Name;
        description.text = w.Description;
        worth.text = w.SellValue.ToString();
        Temp.text = w.Temperature.ToString();
        foreach (var m in w.Recipe.Keys)
        {
            recipe.text += "(" + w.Recipe[m] + ") " + m.Name + "\n";
            
        }

        if (TestLogic.bsg.CheckMetal(w)){
            forge.text="can forge!";
            canForge = true;
        }
        else{
            forge.text = "missing metals";
            canForge = false;

        }
        
    }

	void Update () {
	    if(Input.GetKeyDown(KeyCode.Return)){
            if (canForge) { 
            Application.LoadLevelAdditive("Forgiatura");
            Destroy(gameObject);
            }
        }
	}
}
