﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TestLogic : MonoBehaviour {

	public static BlacksmithGame bsg;

	public Text hDay;
	public Text hHour;
	public Text hGold;
	public Text temperature;

	public TemperatureLevel tempGame;

	public static bool pause = false;

	// Use this for initialization
	void Start () {
		bsg = new BlacksmithGame("GinoLino");
        
  
        bsg.OnGameOver += OnGameOver;
		StartCoroutine(LogicUpdate());
	}
	
	// Update is called once per frame
	void Update () {
		DrawGUI();
	}

    void OnGameOver(){
        TheMessenger.gameover_DAYS = TestLogic.bsg.GameTime.Day;
        TheMessenger.gameover_GLORY = TestLogic.bsg.Fame;
        Application.LoadLevel("GameOver");
     
    }

	IEnumerator LogicUpdate(){
		DrawGUI();
		yield return new WaitForSeconds(1.0f);

		while (true) {
			if (!pause) {
				Debug.Log("Tick");
				bsg.PerformStep();
			}
			yield return new WaitForSeconds(2.5f);
		}
	}

	void DrawGUI() {
		hDay.text = bsg.GameTime.Day.ToString();
		hHour.text = bsg.GameTime.Hour.ToString();
		hGold.text = bsg.Gold.ToString() + "$";
		temperature.text = tempGame.TLevel.ToString();
	}
}
