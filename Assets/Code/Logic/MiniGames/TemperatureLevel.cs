﻿using UnityEngine;
using System.Collections;

public class TemperatureLevel : MonoBehaviour {

	public KeyCode TemperatureKey;
	public float TemperatureDecayRate;
	public float ButtonForce;

	public float TLevel {get; private set; }
	private float intStep;

	// Use this for initialization
	void Start () {
		TLevel = 10;
		intStep = Time.fixedDeltaTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (TemperatureKey)) {
			TLevel += ButtonForce;
		}
	}

	void FixedUpdate() {
		TemperatureDecay ();
	}

	void TemperatureDecay() {
		TLevel = TLevel * Mathf.Exp (-intStep / TemperatureDecayRate);
	}
}
