﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RithmGame : MonoBehaviour {

	public int RubatoProb;

	public bool[] pattern = new bool[32];

	// Use this for initialization
	void Start () {
		//InitializeRandomPattern (5);
		//PrintPattern ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void PrintPattern() {
		string res = "";
		for (int i=0;i<32;i++){
			res = res + pattern[i] + " ";
		}
		Debug.Log(res);
	}

	public void InitializeRandomPattern(int pickAmount) {
		if (pickAmount < 0 || pickAmount > 32) {
			Debug.LogError("Schicchi ha detto NO! (pickAmount out of bound)");
		}

		for (var i = 0; i< pattern.Length; i++) {
			pattern[i] =false;
		}

		int level = 1;

		while (pickAmount > 0) {
			List<int> freeIndexAtLevel = new List<int>();
			int stepAmount = (int) Mathf.Pow(4,level);
			for (var i = 0; i<stepAmount;i++){
				if (!pattern[(32/stepAmount)*i]) {
					freeIndexAtLevel.Add((32/stepAmount)*i);
				}
			}
			if (freeIndexAtLevel.Count==0) {
				level++;
				continue;
			}
			int randIndex = freeIndexAtLevel[Random.Range(0,freeIndexAtLevel.Count)];
			pattern[randIndex] = true;
			pickAmount--;
		}

	}
}
