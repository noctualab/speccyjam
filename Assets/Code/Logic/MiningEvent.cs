﻿using UnityEngine;
using System.Collections;

public class MiningEvent {

	public enum EventType { SUCCESS, AMAZING_SUCCESS, EPIC_WIN, COMBAT_FAILURE, SEVERE_COMBAT_FAILURE,
		CRITICAL_COMBAT_FAILURE, MINING_FAIULURE }

	public string Description { get; private set; }
	public Metal RetrivedMetal { get; private set; }
	public int MetalAmount { get; private set; }
	public EventType Type { get; private set; }

	public static MiningEvent GetSuccess(Metal m) {
		return new MiningEvent() {
			Description = "Test", 
			RetrivedMetal = m, MetalAmount = 1, 
			Type = EventType.SUCCESS
		};
	}

	public static MiningEvent GetMiningFailure() {
		return new MiningEvent() {
			Description = "False",
			RetrivedMetal = null,
			Type = EventType.MINING_FAIULURE
		};
	}

	public static MiningEvent GetAmazingSuccess(Metal m) {
		return new MiningEvent() {
			Description = "Test", 
			RetrivedMetal = m, 
			MetalAmount = 2, 
			Type = EventType.AMAZING_SUCCESS
		};
	}

	public static MiningEvent GetEpicWin(Metal m) {
		return new MiningEvent() {
			Description = "Test", 
			RetrivedMetal = m, 
			MetalAmount = 4, 
			Type = EventType.AMAZING_SUCCESS
		};
	}

	private static MiningEvent GenericCombatFailure(EventType severity) {
		return new MiningEvent() {
			Description = "Test",
			RetrivedMetal = null,
			Type = severity
		};
	}

	public static MiningEvent GetCombatFailure() {
		if (Random.Range(0.0f,1.0f) > 0.02)
			return GenericCombatFailure(EventType.COMBAT_FAILURE);
		return GetSevereCombatFailure();
	}

	public static MiningEvent GetSevereCombatFailure() {
		if (Random.Range(0.0f,1.0f) > 0.02)
			return GenericCombatFailure(EventType.SEVERE_COMBAT_FAILURE);
		return GetCriticalCombatFailure();
	}

	public static MiningEvent GetCriticalCombatFailure() {
		return GenericCombatFailure(EventType.CRITICAL_COMBAT_FAILURE);
	}
}
