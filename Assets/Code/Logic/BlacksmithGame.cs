﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BlacksmithGame {
	BSTime gameTime = new BSTime();
	public List<Minion> minions = new List<Minion>();

	public string Name;

	public List<Minion> AvailableMinion = new List<Minion>();
	public List<Order> Orders = new List<Order> ();
	public Inventory CurrentInventory = new Inventory();

	public BSTimer Timer;
	public int SkillPerFame;

    public int Forged;
    public int Superior;

	public int Gold { get; set; }
	public int Fame { get; set; }

	public delegate void OrderCompletedHandler(Order o);
	public delegate void GameOverHandler();
	public delegate void MiningEventHandler(MiningEvent ev);

	public event OrderCompletedHandler OnOrderCompleted;
	public event GameOverHandler OnGameOver;
	public event MiningEventHandler MiningSuccess;
	public event MiningEventHandler MiningFailure;

	public BSTime GameTime {
		get { return gameTime;}
	}

	public BlacksmithGame(string name) {
		Name = name;
		Gold = 500;
        SkillPerFame = 10;
		Timer = new BSTimer(gameTime);
		gameTime.DayPassed += PayMinions;
		gameTime.DayPassed += RefreshAvailableMinions;
        
        RefreshAvailableMinions();
	}

	public void AddMinion(Minion m) {
		minions.Add(m);
	}

    public int TotalExpenses()
    {
        int total = 0;
        foreach (var m in minions)
        {
            total += m.HireQuote / 12;
        }
        return total;
    }

	public void PerformStep() {
		gameTime.Step();
		Timer.Spin();
	}

	public void PayMinions() {
		foreach (var m in minions) {
			Gold-= m.MonthlyPay;
		}
		if (Gold < 0) {
			if (OnGameOver!=null) OnGameOver();
		}
	}

	public void HireMinion(Minion m) {
		if (!AvailableMinion.Contains (m)) {
			Debug.LogError("Error: Minion Not Available for Hire!");
			return;
		}
		AvailableMinion.Remove (m);
		minions.Add (m);
	}

	public bool CheckAndRemoveInventoryForOrder(Order o) {
		Weapon w = o.OrderedWeapon;
		foreach (var i in CurrentInventory.Contents) {
			if (i is InventoryWeaponSlot) {
				InventoryWeaponSlot iws = i as InventoryWeaponSlot;
				if (iws.StoredWeapon == o.OrderedWeapon) {
					CurrentInventory.Contents.Remove(i);
					return true;
				}
			}
		}
		return false;
	}

    public bool CheckMetal(Weapon w)
    {
        int canForge = 0;
        foreach (var m in w.Recipe.Keys)
        {
            foreach (var i in CurrentInventory.Contents)
            {
                if (i is InventoryMetalSlot)
                {
                    InventoryMetalSlot ims = i as InventoryMetalSlot;
                    if (ims.StoredMetal == m && ims.Amount >= w.Recipe[m])
                    {
                        canForge++;
                    }

                }
            }
        }
        return canForge == w.Recipe.Keys.Count;
    }

	public void CompleteOrder(Order o) {
		if (!Orders.Contains (o)) {
			Debug.LogError("Error: Order Not Found!");
			return;
		}
		if (CheckAndRemoveInventoryForOrder (o)) {
			Orders.Remove (o);
			Fame += o.GainedFame ();
			Gold += (int)(o.OrderedWeapon.SellValue * o.Amount * 1.5f);
			return;
		}
		Debug.LogError ("Order is not satisfiable!");

	}

	public void RefreshAvailableMinions() {
		AvailableMinion.Clear ();
		for (int i=0; i<5; i++) {
			float t = UnityEngine.Random.Range(0.2f,0.8f);
            float p = SkillPerFame * (Fame +1);
            Debug.Log("SKILL " + SkillPerFame);
			AvailableMinion.Add(new Minion((int) (p*t),(int) (p*(1-t))));
		}
	}

	public void ProcessMiningEvent(Minion m, Location l) {
		MiningEvent ev = l.ExtractMetal(m);
		switch (ev.Type) {
		case MiningEvent.EventType.SUCCESS :
			if (MiningSuccess!=null) MiningSuccess(ev);
            CurrentInventory.addMetal(ev.RetrivedMetal, ev.MetalAmount);
			m.Enable();
			break;
		case MiningEvent.EventType.SEVERE_COMBAT_FAILURE :
			if (MiningFailure!=null) MiningFailure(ev);
			minions.Remove(m);
			break;
		default :
			if (MiningFailure!=null) MiningFailure(ev);
			m.Enable();
			break;
		}
	}

	public void SendMinionToLocation(Minion m, Location l) {
		if (!minions.Contains (m) || !m.Available) {
			Debug.LogError("ERROR: Minion Not Available for Work");
			return;
		}
		m.Disable ();

		Timer.RegisterTimer (() => {ProcessMiningEvent (m,l); }, l.ExpeditionDuration);

	}
}

//-------------------------------------------------------------------------
public class BSTime {
	private int day = 0;
	private int hour = 0;

	public event Action DayPassed;

	public int Day {
		get {
			return day+1;
		}
	}

	public int Hour {
		get {
			return hour+1;
		}
	}

	public int GetTimeCode() {
		return day*12+hour;
	}

	public void Step() {
		hour++;
		if (hour>=12) {
			day++;
			hour = 0;
            if(DayPassed!=null)
			DayPassed();
		}
	}

}
