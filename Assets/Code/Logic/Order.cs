﻿using UnityEngine;
using System.Collections;

public class Order {

	public Weapon OrderedWeapon;
	public int Amount;

	public int GainedFame() {
		return OrderedWeapon.SellValue * Amount * 10;
	}

	public static Order GetRanomOrder(int targetFame) {
		int increment = targetFame / 12;
		int randomAmount = Random.Range (1, 5);
		int weaponFame = increment / 10 / randomAmount;

		// Search nearest weapon
		Weapon nw = null;
		float minDistance = 100000.0f;
		foreach (var w in Weapon.Weapons.Values) {
			if (Mathf.Abs(weaponFame - w.SellValue) < minDistance) {
				minDistance = Mathf.Abs(weaponFame - w.SellValue);
				nw = w;
			}
		}

		return new Order () { OrderedWeapon = nw, Amount = randomAmount };
	}

	public string ToString() {
		return OrderedWeapon.Name + " : " + Amount;
	}
}
