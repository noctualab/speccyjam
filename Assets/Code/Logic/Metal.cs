﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Metal {

	public string Name { get; private set; }
	public string Description { get; private set; }
	public int Level { get; private set; }

	private Metal(string name, string desc, int level) {
		Name = name;
		Description = desc;
		Level = level;
	}

	// ----------------------------------------------------------------------------------
	// DATA DRIVEN INITIALIZATION
	public static Dictionary<string,Metal> Metals = new Dictionary<string,Metal>() {
		{"mud",    new Metal("Mud"   ,"It's not metal. But...",1)},
		{"copper", new Metal("Copper","Good conductive metal!",10)},
		{"iron"  , new Metal("Iron"  ,"That's a true metal!"  ,18)},
        {"steel"  , new Metal("Steel"  ,"That's a true metal!"  ,28)},
        {"gold"  , new Metal("Gold"  ,"That's a true metal!"  ,40)},
	};
}
