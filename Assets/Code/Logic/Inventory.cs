﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory  {
	public List<InventorySlot> Contents = new List<InventorySlot> ();
    public bool isMetalPresent(Metal m){
        foreach (var slot  in Contents)
        {
            if(slot is InventoryMetalSlot){
                InventoryMetalSlot metal = slot as InventoryMetalSlot;
                if (metal.StoredMetal == m) return true;
            }
            
        }
        return false;
    }

    public void removeMetal(Metal m, int qnt){
        InventorySlot toBeRemoved=null;
        foreach (var slot in Contents)
        {
            if (slot is InventoryMetalSlot)
            {
                InventoryMetalSlot metal = slot as InventoryMetalSlot;
                if (metal.StoredMetal == m)
                {
                    metal.Amount -= qnt;
                    if (metal.Amount == 0)
                    {
                        toBeRemoved = slot;
                    }
                    break;
                    
                }
            }

        }
        if (toBeRemoved != null)
        {
            Contents.Remove(toBeRemoved);
        }
    }

    public int countWeapons()
    {
        int i = 0;
        foreach (var slot in Contents)
        {
            if (slot is InventoryWeaponSlot)
            {
                i++;
            }

        }
        return i;
    }

    public void addMetal(Metal m, int qnt){
        if (!isMetalPresent(m)) //First resource
        {
            Contents.Add(new InventoryMetalSlot() { StoredMetal = m, Amount = qnt });
        }
        else //already have, increment that
        {
            foreach (var slot in Contents)
            {
                if (slot is InventoryMetalSlot)
                {
                    InventoryMetalSlot metal = slot as InventoryMetalSlot;
                    if (metal.StoredMetal == m)
                    {
                        metal.Amount += qnt;
                    }
                }

            }
        }
    }
}

public class InventorySlot {

}

public class InventoryMetalSlot : InventorySlot {
	public Metal StoredMetal;
	public int Amount;
}

public class InventoryWeaponSlot : InventorySlot {
	public Weapon StoredWeapon;
	public Weapon.Quality WQuality;
}


