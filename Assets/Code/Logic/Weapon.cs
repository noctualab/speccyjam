﻿using UnityEngine;
using System.Collections.Generic;

public class Weapon {

	public string Name { get; private set; }
	public string Description { get; private set; }
	public int SellValue { get; private set; }
    public int Pattern { get; private set; }
    public int Temperature { get; private set; }
	public Dictionary<Metal,int> Recipe;

	public enum Quality { EPIC, NORMAL, TERRIBLE }

	// ----------------------------------------------------------------------------------
	// DATA DRIVEN INITIALIZATION
    public static List<int> WeaponsPerPage = new List<int>(){
        5,2
    };
    public static int NumberOfPages = 2;

	public static Dictionary<string, Weapon> Weapons = new Dictionary<string, Weapon>() {
		{ "mudsword", new Weapon() { 
				Name = "Mud Sword", 
				Description = "A sword of mud!",
				SellValue = 10,
                Pattern = 3,
                Temperature = 100,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["mud"], 1 }
					}
				}
		},

		{ "heavymudsword", new Weapon() { 
				Name = "Heavy Mud Sword", 
				Description = "An heavy sword of mud!",
				SellValue = 30,
                Pattern = 5,
                Temperature = 120,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["mud"], 2 }
				}
			}
		},

        { "rustysword", new Weapon() { 
				Name = "Rusty Sword", 
				Description = "A rusty sword, not so classy",
				SellValue = 40,
                Pattern = 6,
                Temperature = 120,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["copper"], 1 }
					}
				}
		},
        { "rustyironsword", new Weapon() { 
				Name = "Rusty iron Sword", 
				Description = "A rusty sword to slay rats",
				SellValue = 80,
                Pattern = 7,
                Temperature = 130,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["iron"], 1 }
					}
				}
		},
        { "ironsword", new Weapon() { 
				Name = "Iron Sword", 
				Description = "An everyday iron sword",
				SellValue = 150,
                Pattern = 9,
                Temperature = 150,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["iron"], 1 }
					}
				}
		},

        { "shinyironsword", new Weapon() { 
				Name = "Shiny Iron Sword", 
				Description = "An elegant iron sword",
				SellValue = 180,
                Pattern = 5,
                Temperature = 180,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["iron"], 2 }
					}
				}
		},

        
        { "broadsword", new Weapon() { 
				Name = "Broad bicolour Sword", 
				Description = "A fancy big sword",
				SellValue = 250,
                Pattern = 7,
                Temperature = 170,
				Recipe = new Dictionary<Metal, int>() {
					{ Metal.Metals["iron"], 1 },
                    { Metal.Metals["copper"], 1 }
					}
				}
		},
	}; 

}
