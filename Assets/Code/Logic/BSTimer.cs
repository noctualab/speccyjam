﻿using UnityEngine;
using System.Collections.Generic;

public delegate void TimerDelegate();

public class BSTimer
{
	Dictionary<TimerDelegate,int> ActiveTimers;

	BSTime timeInstance;

	public BSTimer(BSTime timeInstance) {
		this.timeInstance = timeInstance;
		ActiveTimers = new Dictionary<TimerDelegate,int>();
	}

	public void RegisterTimer(TimerDelegate action, int time) {
		ActiveTimers.Add(action,time + timeInstance.GetTimeCode());
	}

	public void Spin() {
		List<TimerDelegate> used = new List<TimerDelegate>();
		foreach (var t in ActiveTimers.Keys) {
			if (timeInstance.GetTimeCode() == ActiveTimers[t]) {
				t();
				used.Add(t);
			}
		}
		// Remove used timers.
		foreach (var u in used) {
			ActiveTimers.Remove(u);
		}
	}
}
