﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Location {

	public string Name { get; private set; }
	public int EnemyLevel { get; private set;}
	public Dictionary<string,float> PossibleMetals;
	public int MaxMetalLevel;
	public int ExpeditionDuration;

	// This value can be found imposing the following codition. We want
	// that the success probability for a minion wit C (combat level) x against 
	// a location with EL (enemy level) x is about 90%. Or in math
	//
	//    1 / (1 + e^-(DELTA + eventSuccessFactor)) = 0.9
	//
	// where DELTA = (C-EL)/SCALE
	private const float evenSuccessFactor = 2.19f;
	private const float scale = 10.0f;

	public float GetCombatProbability(Minion m) {
		float deltaC = (m.Combat - EnemyLevel) / scale;
		float combatSuccess = 1.0f / (1 + Mathf.Exp(-(deltaC + evenSuccessFactor)));
		return combatSuccess;
	}

	public float GetMiningProbability(Minion m, int metalLevel) {
		float deltaG = (m.Geology - metalLevel) / scale;
		float geologySuccess = 1.0f / (1 + Mathf.Exp(-(deltaG + evenSuccessFactor)));
		return geologySuccess;
	}

	public float EstimateSuccess(Minion m) {
		return GetCombatProbability(m) * GetMiningProbability(m,MaxMetalLevel);
	}

	public MiningEvent ExtractMetal(Minion m) {
		// Check Combat
		float combatProb = GetCombatProbability(m);
		float combatThreshold = 1 - combatProb;
		float combatDice = Random.Range(0.0f,1.0f);
		Debug.Log("Threshold = " + combatThreshold);
		if (combatDice >= combatThreshold) {
			Debug.Log("Combat Success (" + combatDice + ")");
		} else if (combatThreshold - combatDice < 0.2f) {
			Debug.Log("Minor Combat Failure (" + combatDice + ")");
			return MiningEvent.GetCombatFailure();
		} else if (combatThreshold - combatDice < 0.4f) {
			Debug.Log("Severe Combat Failure (" + combatDice + ")");
			return MiningEvent.GetSevereCombatFailure();
		} else {
			Debug.Log("Critical Combat Faiulure (" + combatDice + ")");
			return MiningEvent.GetCriticalCombatFailure();
		}

		// Check Metal
		float metalDice = Random.Range(0.0f,1.0f);
		string selectedMetal = "";
		foreach (var ms in PossibleMetals.Keys) {
			metalDice -= PossibleMetals[ms];
			if (metalDice < 0) {
				selectedMetal = ms;
			}
		}

		float miningDice = Random.Range(0.0f,1.0f);
		float miningThreshold = 1 - GetMiningProbability(m,Metal.Metals[selectedMetal].Level);
		if (miningDice >= miningThreshold) {
			Debug.Log("Mining Success (" + miningDice + ")");
			return MiningEvent.GetSuccess(Metal.Metals[selectedMetal]);
		}
		return MiningEvent.GetMiningFailure();
	}

//	private Location(string name, int enemy, int metals) {
//		Name = name;
//		EnemyLevel = enemy;
//		MaxMetalLevel = metals;
//	}

	// ----------------------------------------------------------------------------------
	// DATA DRIVEN INITIALIZATION
	public static Dictionary<string,Location> Locations = new Dictionary<string,Location>() {
		{"polloria",    new Location() { 
				Name = "ChickenVille", 
				EnemyLevel = 5, 
				MaxMetalLevel = 1,
				ExpeditionDuration = 10,
				PossibleMetals = new Dictionary<string,float> { { "mud", 1.0f} }
			}
		},
        {"terraria",    new Location() { 
				Name = "Shadowhurst", 
				EnemyLevel = 15, 
				MaxMetalLevel = 10,
				ExpeditionDuration = 20,
				PossibleMetals = new Dictionary<string,float> { { "copper", 1.0f} }
			}
		},
        {"montemorte",    new Location() { 
				Name = "Holt Mountain", 
				EnemyLevel = 30, 
				MaxMetalLevel = 18,
				ExpeditionDuration = 30,
				PossibleMetals = new Dictionary<string,float> { { "iron", 1.0f} }
			}
		},
        {"subaugusta",    new Location() { 
				Name = "Subaugusta", 
				EnemyLevel = 45, 
				MaxMetalLevel = 28,
				ExpeditionDuration = 45,
				PossibleMetals = new Dictionary<string,float> { { "steel", 1.0f} }
			}
		},
        {"pigneto",    new Location() { 
				Name = "Tristran", 
				EnemyLevel = 65, 
				MaxMetalLevel = 40,
				ExpeditionDuration = 60,
				PossibleMetals = new Dictionary<string,float> { { "gold", 1.0f} }
			}
		},
	};
}
