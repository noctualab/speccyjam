﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class gameOver : MonoBehaviour {

	// Use this for initialization
    public Text glory;
    public Text days;
	void Start () {
        TestLogic.pause = true;
        days.text = TheMessenger.gameover_DAYS.ToString();
        glory.text = TheMessenger.gameover_GLORY.ToString();
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
